---
title: "Medway_data_explore"
author: "Ed Flahavan"
date: "10/10/2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Load packages

```{r}
require(lubridate)
require(tidyverse)
require(reshape2)
```


## Loading data

This reads in all of the data serie.

```{r}
# Set working directory and read in all the csv files
setwd("~/Desktop/TS/medway_data/raw")
filenames <- list.files(full.names=TRUE, pattern = "*.csv")  

# Read in each csv and convert the date to a date_time_varible for each
all_data <- lapply(filenames,function(i){
 df =  read.csv(i, header=TRUE)
 df$date_data = ymd_hms(df$date_data)
 df
})

## Tidy up all of the datasets in the order they appear in the list
filenames

# Ambulances conveyed
ambs_conveyed <- data.frame(all_data[1])
colnames(ambs_conveyed) = c("ams_conveyed", "date_time")

# Maximum number of patients in the department per hour
patients_dept_hourly_max <- data.frame(all_data[2])
colnames(patients_dept_hourly_max) = c("patients_dept_hourly_max", "date_time")
 
# Cumulative number of attendences 
attendences_hourly_cumulative <- data.frame(all_data[3])
colnames(attendences_hourly_cumulative) = c("attendences_hourly_cumulative", "date_time")

# Number major attendences
majors_attendences_hourly_cumulative <- data.frame(all_data[4])
colnames(majors_attendences_hourly_cumulative ) = c("majors_attendences_hourly_cumulative ", "date_time")

# Minors attendences hourly
minors_attendences_hourly  <- data.frame(all_data[5])
colnames(minors_attendences_hourly) = c("minors_attendences_hourly", "date_time")

# Resus cubicles available
resus_cubs_available <- data.frame(all_data[6])
colnames(resus_cubs_available) = c("resus_cubs_available", "date_time")

# Acute beds available
beds_available_acute <- data.frame(all_data[7])
colnames(beds_available_acute) = c("beds_available_acute", "date_time")

# Unallocated patients in the ED
unallocated_patients_in_ed = data.frame(all_data[8])
colnames(unallocated_patients_in_ed) = c("unallocated_patients_in_ed", "date_time")

# Wait in ED
wait_hourly <-  data.frame(all_data[9])
colnames(wait_hourly) = c("wait_hourly", "date_time")

# Wait in ED for majors
wait_hourly_majors <-  data.frame(all_data[10])
colnames(wait_hourly_majors ) = c("wait_hourly_majors", "date_time")

# Wait in ED for minors
wait_hourly_minors <-  data.frame(all_data[11])
colnames(wait_hourly_minors) = c("wait_hourly_minors", "date_time")


```


In order to merge all of the data together create a vector of hours from the minimum we have in the dataset to the max.

```{r}

min_date_time = min( ambs_conveyed$date_time )
max_date_time = max( ambs_conveyed$date_time)

# Create sequence of all the hours in the data
full_hours = data.frame( date_time =  seq(from = min_date_time, to = max_date_time, by = 3600 ))

```

Merge in all of the time series.

```{r}
## Left join all datasets into one
master <- list(full_hours ,ambs_conveyed, attendences_hourly_cumulative, beds_available_acute,
     majors_attendences_hourly_cumulative, minors_attendences_hourly,
     patients_dept_hourly_max, resus_cubs_available, unallocated_patients_in_ed,
     wait_hourly, wait_hourly_majors, wait_hourly_minors) %>%
    Reduce(function(dtf1,dtf2) left_join(dtf1,dtf2,by="date_time"), .)

summary(master)

```


## Create Lagged and rolling means of each variable

Drop the period before which we only have ambulance data. That is before 2016-09-30 10:00:00

Create lagged variables for 12, 24 and 48 hours for each variable.

And the create rolling means 6, 12, 24 and 48 hours for each variable including all lags.


```{r}
require(xts)
master = filter(master, date_time > ymd_hms("2016-09-30 10:00:00") )

master_xts = xts(master[, -1], order.by = master$date_time)

## Create all of the lagged indicators

for(i in 1:length(colnames(master_xts))){
  
  # Isolate the column name
  colname_i = colnames(master_xts)[i]
  
  # Create the lagged indicators 12 hours
  master_xts$temp_name1 =  lag.xts( master_xts[, colname_i], k = 12, lag = 1)
  names(master_xts)[names(master_xts) == 'temp_name1'] = paste0(colname_i, "_lag_12hr")
  
  # Create the lagged indicators 24 hours
  master_xts$temp_name2 =  lag.xts( master_xts[, colname_i], k = 24, lag = 1)
  names(master_xts)[names(master_xts) == 'temp_name2'] = paste0(colname_i, "_lag_24hr")
  
  # Create the lagged indicators 48 hours
  master_xts$temp_name3 =  lag.xts( master_xts[, colname_i], k = 48, lag = 1)
  names(master_xts)[names(master_xts) == 'temp_name3'] = paste0(colname_i, "_lag_48hr")
  
  
}


## Now create rolling means for all indicators
for(i in 1:length(colnames(master_xts))){
  
  # Isolate the column name
  colname_i = colnames(master_xts)[i]
  
  # Create the 6 hour rolling mean
  master_xts$temp_name1 = rollapply(   master_xts[, colname_i],
                                       width = 6,
                                       FUN=function(x) mean(x, na.rm=TRUE),
                                        by=1, by.column=TRUE,
                                       fill=NA, align="right" )
  names(master_xts)[names(master_xts) == 'temp_name1'] = paste0(colname_i, "_6hr_rollmean")
  
  # Create the 12 hour rolling mean
    master_xts$temp_name2 = rollapply(   master_xts[, colname_i],
                                       width = 12,
                                       FUN=function(x) mean(x, na.rm=TRUE),
                                        by=1, by.column=TRUE,
                                       fill=NA, align="right" )
  names(master_xts)[names(master_xts) == 'temp_name2'] = paste0(colname_i, "_12hr_rollmean")
  
  # Create the 24 hour rolling mean
  master_xts$temp_name3 = rollapply(   master_xts[, colname_i],
                                       width = 24,
                                       FUN=function(x) mean(x, na.rm=TRUE),
                                        by=1, by.column=TRUE,
                                       fill=NA, align="right" )
  names(master_xts)[names(master_xts) == 'temp_name3'] = paste0(colname_i, "_24hr_rollmean")
  
  # Create the 48 hour rolling mean
  master_xts$temp_name4 = rollapply(   master_xts[, colname_i],
                                       width = 48,
                                       FUN=function(x) mean(x, na.rm=TRUE),
                                        by=1, by.column=TRUE,
                                       fill=NA, align="right" )
  names(master_xts)[names(master_xts) == 'temp_name4'] = paste0(colname_i, "_48hr_rollmean")
  
  
}

```

## Calculate time windows

Next step is to create the file with the time windows and max and mean within those

```{r}
master_df = data.frame(master_xts)

master_df$date_time = master$date_time

# Create the time intervals
master_df = master_df %>% 
  mutate( time_window = ifelse(hour(date_time) < 6, 1, 
                               ifelse(hour(date_time) < 12, 2,
                                      ifelse(hour(date_time) < 18, 3, 4)))) 

# check that there are an equal number of observations in each window
table(master_df$time_window)

# This looks good

# We can take a look at how these seem to vary over different days of the week
master_df %>% ungroup() %>%
  group_by(wday = wday(date_time), window = time_window ) %>%
  summarise(mean_wtbs = mean(wait_hourly, na.rm = T))


# Nw we want to compute mean and max in that time window
# on that day for each observation
master_df = master_df %>% ungroup() %>%
  group_by(date = date(date_time), window = time_window ) %>%
  mutate(window_mean_wtbs = mean(wait_hourly, na.rm = T),
         window_max_wtbs = max(wait_hourly)) %>%
  ungroup() %>% 
  mutate(window_mean_wtbs_ntile = ntile(window_mean_wtbs, 100  ),
         window_max_wtbs_ntile = ntile(window_max_wtbs, 100))

# View(dplyr::select(master_df, date_time, wait_hourly, window_mean_wtbs,
#                    window_max_wtbs, window_mean_wtbs_ntile, window_max_wtbs_ntile))
```

## plots of data

Few quick plots of the data


```{r}
ggplot(master_df, aes(window_mean_wtbs)) +
  geom_histogram() + ggtitle("histogram of mean waiting time in that 6 hour window")

ggplot(master_df, aes(window_max_wtbs)) +
  geom_histogram() + ggtitle("histogram of maximum waiting time in that 6 hour window")

# df_2 = master_df %>% 
#   group_by(date = date(date_time), window = time_window) %>%
#   summarise(window_mean_wtbs_ntile = first(window_mean_wtbs_ntile))
#   
```

Relationship between mean waiting time and max waiting time. 

```{r}
ggplot(master_df, aes(window_max_wtbs, window_mean_wtbs)) +
  geom_point() + ggtitle("Scatter of max against mean")



ggplot(master_df, aes(window_max_wtbs_ntile, window_mean_wtbs_ntile)) +
  geom_point() + ggtitle("Scatter of percentiles of max against mean")


```


This relationship isn't as tight as I might have expected - a bad day as measured by the max waiting time percentile is not necessarily bad as measured by the mean.


## Write to a csv

```{r}
write.csv(x = master_df, file = "medway_data_prepped.csv")
```

